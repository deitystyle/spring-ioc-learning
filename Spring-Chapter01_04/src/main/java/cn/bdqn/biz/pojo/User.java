package cn.bdqn.biz.pojo;

/**
 * 用户实体类
 */
public class User{
	private int id; // 用户ID
	private String userName; // 用户名
	private String password; // 密码
	private String email; // 电子邮件
	
	//注意：一定要保留此默认无参构造方法
	public User(){
	}
	
	public User(int id,String userName,String password,String email){
		this.id = id;
		this.userName = userName;
		this.email = email;
		this.password = password;
	}
	
	// getter & setter
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
