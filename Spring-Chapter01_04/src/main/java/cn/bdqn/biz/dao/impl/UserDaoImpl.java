package cn.bdqn.biz.dao.impl;

import cn.bdqn.biz.dao.UserDao;
import cn.bdqn.biz.pojo.User;

/**
 * 用户DAO实现类，实现UserDao接口，负责User类的持久化操作
 */
public class UserDaoImpl implements UserDao {

	public void save(User user) {
		// 这里并未实现完整的数据库操作，仅为说明问题
		System.out.println("保存用户信息到数据库："+user.getUserName());
	}
	
}
