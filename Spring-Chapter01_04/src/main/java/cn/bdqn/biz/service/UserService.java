package cn.bdqn.biz.service;

import cn.bdqn.biz.pojo.User;

/**
 * 用户业务接口,定义了所需的业务方法
 */
public interface UserService {
	public void addNewUser();
}
