package cn.bdqn.biz.service.impl;

import cn.bdqn.biz.dao.UserDao;
import cn.bdqn.biz.pojo.User;
import cn.bdqn.biz.service.UserService;
/**
 * 用户业务类,实现对User功能的业务管理
 */
public class UserServiceImpl implements UserService {

	    // 声明接口类型的引用,和具体实现类解耦合
		private UserDao userDao;
		private User user;

		public UserServiceImpl(){
		}
		
		public UserServiceImpl(UserDao userDao){
			this.userDao = userDao;
		}
		
		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		// userDao 属性的setter访问器,会被Spring调用,实现设值注入
		public void setUserDao(UserDao userDao) {
			this.userDao = userDao;
		}

		public void addNewUser() {
			// 调用用户DAO的方法保存用户信息
			userDao.save(user);
		}

}
