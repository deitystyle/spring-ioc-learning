package test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import entity.TestEntity;

public class Test {

	public static void main(String[] args) {
		// 使用ApplicationContext接口的实现类ClassPathXmlApplicationContext加载Spring配置文件
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		TestEntity entity = (TestEntity) ctx.getBean("entity");
		entity.showValue();
	}

}
