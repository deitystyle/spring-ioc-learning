package cn.bdqn.biz;

//移动硬盘
public class ConDisk implements Disk{
	
	// 定义data属性，该属性的值将通过Spring框架进行设置
	private String data;

	// data 属性的setter访问器,会被Spring调用,实现设值注入
	public void setData(String data) {
		this.data = data;
	}
	
	public String getData() {
		return data;
	}
	
	@Override
	public void read() {
		// TODO Auto-generated method stub
		System.out.println("移动硬盘读取数据！");
	}

	@Override
	public void write() {
		// TODO Auto-generated method stub
		System.out.println("移动硬盘写入数据："+this.getData());
	}

}
