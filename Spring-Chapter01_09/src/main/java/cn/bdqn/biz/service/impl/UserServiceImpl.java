package cn.bdqn.biz.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.bdqn.biz.dao.UserDao;
import cn.bdqn.biz.pojo.User;
import cn.bdqn.biz.service.UserService;
/**
 * 用户业务类,实现对User功能的业务管理
 */
@Service("userServiceImpl")
public class UserServiceImpl implements UserService {
		
	
	    @Autowired
		private UserDao userDao;

		public UserDao getUserDao() {
			return userDao;
		}

		// userDao 属性的setter访问器,会被Spring调用,实现设值注入
		public void setUserDao(UserDao userDao) {
			this.userDao = userDao;
		}

		public void addNewUser(User user) {
			// 调用用户DAO的方法保存用户信息
			userDao.save(user);
		}

}
