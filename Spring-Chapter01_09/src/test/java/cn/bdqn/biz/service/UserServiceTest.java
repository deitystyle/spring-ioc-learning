package cn.bdqn.biz.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.bdqn.biz.pojo.User;


public class UserServiceTest {

	@Test
	public void test() {
		// 使用ApplicationContext接口的实现类ClassPathXmlApplicationContext加载Spring配置文件
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		// 通过ApplicationContext接口的getBean()方法获取id或name为user的Bean实例
		User user = (User) ctx.getBean("user");
		user.setId(1);
		user.setUserName("test");
		user.setPassword("123456");
		user.setEmail("test@pbdevj.com");
		
		// 通过ApplicationContext接口的getBean()方法获取id或name为userServiceImpl的Bean实例
		UserService userService = (UserService) ctx.getBean("userServiceImpl");
		userService.addNewUser(user);
		
	}

}
