package test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import entity.User;

public class Test {

	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		System.out.println("================singleton================");
		User user1 = (User) ctx.getBean("singletonUser");
		System.out.println("user1: " + user1.getUsername()); // 张三
		user1.setUsername(null);

		User user2 = (User) ctx.getBean("singletonUser");
		System.out.println("user2: " + user2.getUsername()); // null

		System.out.println(user1 == user2); // true
		System.out.println("================prototype================");
		User user3 = (User) ctx.getBean("prototypeUser");
		System.out.println("user3: " + user3.getUsername()); // 李四
		user3.setUsername(null);

		User user4 = (User) ctx.getBean("prototypeUser");
		System.out.println("user4: " + user4.getUsername()); // 李四

		System.out.println(user3 == user4); // false
	}


}
