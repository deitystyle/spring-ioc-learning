package cn.bdqn.biz.service;

import static org.junit.Assert.*;

import org.junit.Test;

import cn.bdqn.biz.dao.Disk;

public class BeanFactoryTest {

	@Test
	public void testGetBean() {
		Disk disk = (Disk)BeanFactory.getBean("cn.bdqn.biz.dao.impl.UsbDisk");
		disk.read();
		disk.write();
	}

}
