package cn.bdqn.biz.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.bdqn.biz.dao.Disk;

public class DiskTest {

	@Test
	public void testRead() {
//		Disk disk = new ConDisk();
//		disk.read();
		
		// 通过ClassPathXmlApplicationContext实例化Spring的上下文
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		// 通过ApplicationContext的getBean()方法，根据id来获取bean的实例(工厂模式来实现)
		Disk disk = (Disk) context.getBean("disk");
		
		disk.read();
		
	}
	
	@Test
	public void testWrite() {
//		Disk disk = new ConDisk();
//		disk.write("hello,Spring!");
		
		// 通过ClassPathXmlApplicationContext实例化Spring的上下文
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		// 通过ApplicationContext的getBean()方法，根据id来获取bean的实例(工厂模式来实现)
		Disk disk = (Disk) context.getBean("disk");
		disk.write();
	}
}
