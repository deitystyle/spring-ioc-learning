package cn.bdqn.biz.service;

public class BeanFactory {
	//获得一个Bean实例的静态方法
	public static Object getBean(String beanName){ 
		
		try {
			return Class.forName(beanName).newInstance();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		
	}
}
