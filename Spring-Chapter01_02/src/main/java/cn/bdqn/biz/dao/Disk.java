package cn.bdqn.biz.dao;

//所有磁盘设备的接口
public interface Disk {
	//读取数据
	public void read();
	//写入数据
	public void write();
}
