package cn.bdqn.biz.pojo;

/**
 * 用户实体类
 */
public class User{
	private Integer id; // 用户ID
	private String userName; // 用户名
	private String password; // 密码
	private String email; // 电子邮件

	// getter & setter
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
