package tool;

import entity.User;

public class UserShow {
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void show() {
		System.out.println("姓名：" + user.getUsername());
		System.out.println("邮箱：" + user.getEmail());
		System.out.println("年龄：" + user.getAge());
	}
}
